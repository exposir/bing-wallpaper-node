## 2022-10-22 云山雾绕  

中国桂林漓江国家公园的喀斯特山脉 (© Sean Pavone/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.KarstMountains_ZH-CN4719178982_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.KarstMountains_ZH-CN4719178982_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-21 美国南部的别样秋景  

秋季的落羽杉，美国佐治亚州 (© Chris Moore/Tandem Stills + Motion) [4k Edition](https://cn.bing.com//th?id=OHR.GeorgiaCypress_ZH-CN3705257154_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.GeorgiaCypress_ZH-CN3705257154_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-20 到哪都挂着  

塔拉曼卡旧港的霍氏树懒母子，哥斯达黎加 (© Suzi Eszterhas/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.SlothDay_ZH-CN4945330735_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SlothDay_ZH-CN4945330735_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-19 等等，这山上得有座城堡！  

秋天的图林根森林与瓦特堡城堡，德国 (© ezypix/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.WartburgCastle_ZH-CN4201605751_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.WartburgCastle_ZH-CN4201605751_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-18 风之精灵  

Bridalveil Fall, Yosemite National Park, California (© Jeff Foott/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.BridalVeilFalls_ZH-CN3954641670_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BridalVeilFalls_ZH-CN3954641670_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-17 “长耳”猫头鹰  

捷克共和国波希米亚-摩拉维亚高地的长耳鸮 (© Ondrej Prosicky/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.SwedenOwl_ZH-CN6960032096_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SwedenOwl_ZH-CN6960032096_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-16 一座北部岛屿的南端  

克里斯蒂安王子之声，格陵兰岛 (© Posnov/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.PrinceChristianSound_ZH-CN0274463143_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PrinceChristianSound_ZH-CN0274463143_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-15 国际考古日  

纳克什鲁斯塔姆遗址，伊朗波斯波利斯 (© mshirani/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.NaqsheRustam_ZH-CN9695151436_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.NaqsheRustam_ZH-CN9695151436_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-14 通往伊比利亚的门户  

奥尔德萨和佩尔迪多山国家公园里的瀑布，西班牙比利牛斯山脉 (© David Santiago Garcia/Cavan Images) [4k Edition](https://cn.bing.com//th?id=OHR.RioArazas_ZH-CN9451571402_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.RioArazas_ZH-CN9451571402_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-13 驼鹿发情的季节  

迪纳利国家公园里的两只驼鹿，美国阿拉斯加州 (© Yva Momatiuk and John Eastcott/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.AlaskaMoose_ZH-CN9148253690_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.AlaskaMoose_ZH-CN9148253690_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-12 回到过去  

蒙茅斯海滩的菊石路面，英国多塞特侏罗纪海岸世界遗产地 (© AWL Images/Danita Delimont) [4k Edition](https://cn.bing.com//th?id=OHR.AmmoniteGraveyard_ZH-CN8904427525_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.AmmoniteGraveyard_ZH-CN8904427525_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-11 放大镜下的墙藓  

带着闪闪发光水滴的泛生墙藓, 荷兰 (© Arjan Troost/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.TortulaMoss_ZH-CN8695265186_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.TortulaMoss_ZH-CN8695265186_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-10 穿越意大利峡湾之旅  

瓦伦蒂诺大坝，意大利伦巴第大区布雷西亚省 (© wmaster890/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.ValvestinoDam_ZH-CN8397604653_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.ValvestinoDam_ZH-CN8397604653_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-9 什么生长得这样茂盛？  

楚科奇海的浮游植物水华，美国阿拉斯加州海岸附近 (© Norman Kuring/Kathryn Hansen/U.S. Geological Survey/NASA) [4k Edition](https://cn.bing.com//th?id=OHR.ChukchiSea_ZH-CN7218471261_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.ChukchiSea_ZH-CN7218471261_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-8 透明的头足类动物  

佛得角附近大西洋中的玻璃章鱼 (© Solvin Zankl/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.GlassOctopus_ZH-CN6853414529_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.GlassOctopus_ZH-CN6853414529_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-7 英国中部最美丽的风景  

温德米尔湖的风景，坎布里亚湖区，英格兰 (© Chris Warren/eStock Photo) [4k Edition](https://cn.bing.com//th?id=OHR.WindermereHills_ZH-CN6614218161_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.WindermereHills_ZH-CN6614218161_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-6 隐入大海的龙尾  

巴斯克海岸世界地质公园的比斯开湾，西班牙 (© Olimpio Fantuz/eStock Photo) [4k Edition](https://cn.bing.com//th?id=OHR.BayofBiscay_ZH-CN6002214693_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BayofBiscay_ZH-CN6002214693_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-5 列队！  

加勒比海红鹳，里奥拉加托斯自然保护区，墨西哥 (© Claudio Contreras/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.FlamingoTeacher_ZH-CN5688509752_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.FlamingoTeacher_ZH-CN5688509752_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-4 今天我们去爬山吧  

黄山的日出，中国 (© zhouyousifang/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.ChongyangFestival_ZH-CN5260976551_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.ChongyangFestival_ZH-CN5260976551_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-3 仙境之旅  

仙女谷，苏格兰斯凯岛 (© e55evu/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.FairyGlen_ZH-CN4521633106_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.FairyGlen_ZH-CN4521633106_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-2 切斯瑞湖  

倒映在湖中的勃朗峰山脉，法国霞慕尼市 (© Stefan Huwiler/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.LacChesserys_ZH-CN4136691056_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.LacChesserys_ZH-CN4136691056_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-10-1 祖国生日快乐！  

山上的日出，河北蔚县 (© zhao zhenhao/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.NationalDay2022_ZH-CN3861603311_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.NationalDay2022_ZH-CN3861603311_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-30 南冰洋里的巨鲸  

在新湾潜水的南露脊鲸，阿根廷瓦尔德斯半岛 (© Gabriel Rojo/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.EubalaenaAustralis_ZH-CN3366455170_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.EubalaenaAustralis_ZH-CN3366455170_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-29 落基山脉的宝石  

约翰斯顿峡谷，加拿大班夫国家公园 (© Jason Hatfield/TANDEM Stills + Motion) [4k Edition](https://cn.bing.com//th?id=OHR.JohnstonWater_ZH-CN3121890365_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.JohnstonWater_ZH-CN3121890365_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-21 在西尔特的最南端  

赫努姆的茅草屋顶, 德国叙尔特岛 (© Francesco Carovillano/eStock Photo) [4k Edition](https://cn.bing.com//th?id=OHR.SyltNordseeHoernum_ZH-CN6316415332_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SyltNordseeHoernum_ZH-CN6316415332_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-20 海上泰迪熊  

阿拉斯加州锡特卡海峡的海獭，美国 (© Robert Harding/Offset/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.SitkaOtters_ZH-CN4715326633_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SitkaOtters_ZH-CN4715326633_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-19 山峦间的光辉之城  

巴斯利卡塔的卡斯泰尔梅扎诺村，意大利 (© Roberto Moiola/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.SanMartinoVillage_ZH-CN4623104087_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SanMartinoVillage_ZH-CN4623104087_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-18 沉浸在大自然中  

幽鹤国家公园的翡翠湖, 加拿大不列颠哥伦比亚省 (© Cavan Images/Offset) [4k Edition](https://cn.bing.com//th?id=OHR.EmeraldYoho_ZH-CN4524610330_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.EmeraldYoho_ZH-CN4524610330_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-17 布莱克浦的灯光太棒了  

布莱克浦塔和中央码头，英国兰开夏郡 (© Bailey-Cooper Photography/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.BlackpoolBeach_ZH-CN2646268897_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BlackpoolBeach_ZH-CN2646268897_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-16 敏捷而隐秘  

百内国家公园中的一头美洲狮，智利巴塔哥尼亚 (© Ingo Arndt/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.PianePuma_ZH-CN1482049046_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PianePuma_ZH-CN1482049046_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-15 从天而降的魔法  

罗兰豁口上空的银河，法国上比利牛斯省 (© SPANI Arnaud/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.PyreneesPark_ZH-CN1341030921_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PyreneesPark_ZH-CN1341030921_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-14 横跨峡谷裂缝的两座桥  

大理石峡谷中横跨科罗拉多河的纳瓦霍桥，美国亚利桑那州北部  (© trekandshoot/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.MarbleCanyon_ZH-CN1066862981_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MarbleCanyon_ZH-CN1066862981_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-13 如沙漏中的沙子一般  

大沙丘国家公园和保护区，美国科罗拉多州 (© Y Paudel/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.GSDNPest_ZH-CN0818304791_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.GSDNPest_ZH-CN0818304791_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-12 一种巨嘴鸟  

栗耳簇舌巨嘴鸟，巴西潘塔纳尔保护区里 (© Ana Gram/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.Aracari_ZH-CN0383753817_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.Aracari_ZH-CN0383753817_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-11 椰林国  

在喀拉拉邦河流中行驶的一艘小船，印度 (© Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.KeralaIndia_ZH-CN0125201857_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.KeralaIndia_ZH-CN0125201857_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-10 海上生明月，天涯共此时  

中秋之夜, 上海豫园 (© Zyxeos30/Getty images) [4k Edition](https://cn.bing.com//th?id=OHR.MidAutumn2022_ZH-CN9825550508_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MidAutumn2022_ZH-CN9825550508_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-9 另一个大堡礁  

伯利兹堡礁保护区, 伯利兹 (© Tom Till/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.BHNMBelize_ZH-CN9422261941_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BHNMBelize_ZH-CN9422261941_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-8 500年前，是谁回到了这座城市？  

从塞维利亚的都市阳伞俯瞰城市，西班牙 (© LucVi/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.CircumnavigationAnni_ZH-CN6835512993_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.CircumnavigationAnni_ZH-CN6835512993_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-7 看起来很锋利的岩石  

英格兰怀特岛上的尼德尔斯白垩岩石和19世纪的灯塔。 (© CBCK Christine/iStock/Getty Images Plus) [4k Edition](https://cn.bing.com//th?id=OHR.TheNeedles_ZH-CN6578835963_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.TheNeedles_ZH-CN6578835963_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-6 一只有趣的小家伙  

叼着蘑菇的红松鼠 (© Michael Quinton/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.SquirrelMushroom_ZH-CN2854383605_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SquirrelMushroom_ZH-CN2854383605_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-5 一次偏远的旅途  

北魁北克的公路，加拿大 (© Posnov/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.TaigaRoad_ZH-CN2567537158_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.TaigaRoad_ZH-CN2567537158_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-4 印度的里维埃拉  

果阿邦的阿兰博尔海滩，印度 (© Ben Pipe/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.ArambolBeach_ZH-CN2149857876_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.ArambolBeach_ZH-CN2149857876_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-3 一双高耸入云的摩天大楼  

吉隆坡石油双塔，马来西亚 (© tampatra/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.MalaysiaTwinTowers_ZH-CN1989513449_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MalaysiaTwinTowers_ZH-CN1989513449_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-2 这个神奇的海滩值得一游  

克里特岛上的海滩，希腊 (© Georgios Tsichlis/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.SeitanLimania_ZH-CN3831790369_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SeitanLimania_ZH-CN3831790369_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-9-1 谁在使用这座绿草如茵的桥？  

维尔登的野生动物通道，荷兰 (© Frans Lemmens/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.WildlifeCrossing_ZH-CN1493053695_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.WildlifeCrossing_ZH-CN1493053695_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-31 漂在海里的蓝色星星  

新爱尔兰岛上的蓝指海星，巴布亚新几内亚 (© Jurgen Freund/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.BlueLinckia_ZH-CN1103817183_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BlueLinckia_ZH-CN1103817183_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-30 比萨不仅有斜塔  

米利亚里诺自然公园，圣罗索尔，意大利马萨西犹可利 (© Stefano Valeri/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.Migliarino_ZH-CN0744250844_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.Migliarino_ZH-CN0744250844_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-29 像湖一样的大海  

爱沙尼亚波罗的海 (© fotoman-kharkov/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.EstoniaBaltic_ZH-CN0314555299_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.EstoniaBaltic_ZH-CN0314555299_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-28 这鸟长着小胡子  

荷兰弗莱福兰湿地的文须雀 (© Gert-Jan IJzerman/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.BeardedTit_ZH-CN0065279700_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BeardedTit_ZH-CN0065279700_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-27 40年的休养生息  

圣海伦斯山国家火山纪念区的边界小径，美国华盛顿州 (© Don Geyer/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.MSHV_ZH-CN9630204701_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MSHV_ZH-CN9630204701_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-26 既有风，又有水  

克罗地亚佩列沙茨半岛附近的风筝冲浪者和风帆冲浪者 (© helivideo/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.PeljesacWind_ZH-CN9299214248_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PeljesacWind_ZH-CN9299214248_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-25 令人惊叹的后花园  

华盛顿州北瀑布国家公园，美国 (© Ethan Welty/Tandem Stills + Motion) [4k Edition](https://cn.bing.com//th?id=OHR.CascadesNP_ZH-CN1830542356_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.CascadesNP_ZH-CN1830542356_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-24 里约热内卢的象征  

格洛里亚附近的码头和糖面包山，巴西里约热内卢 (© f11photo/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.MarinaDaGloria_ZH-CN6894795645_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MarinaDaGloria_ZH-CN6894795645_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-23 到了芒通，就来点柠檬汁吧  

芒通，法国 (© Flavio Foglietta/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.MentonFrance_ZH-CN5849270429_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MentonFrance_ZH-CN5849270429_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-22 猫头鹰界的叛逆小鸟  

南佛罗里达的一只穴小鸮雏鸟和一只成年穴小鸮，美国 (© Carlos Carreno/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.TenderMoment_ZH-CN5447705408_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.TenderMoment_ZH-CN5447705408_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-21 死亡海岸  

加利西亚省科斯塔达莫尔特的灯塔，西班牙 (© Carlos Fernandez/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.CostadaMorte_ZH-CN5219249535_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.CostadaMorte_ZH-CN5219249535_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-20 保护蜜蜂、赞美蜜蜂  

西班牙阿斯图里亚斯省穆涅略斯自然保护区的蜂箱 (© ABB Photo/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.BearProof_ZH-CN4950171791_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.BearProof_ZH-CN4950171791_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-19 彭赞斯全景  

康沃尔郡的彭赞斯，英国 (© Murray Bosley Photography/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.PenzancePool_ZH-CN4493022613_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PenzancePool_ZH-CN4493022613_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-18 世界最臭食物之乡？  

哥德堡群岛的岸边，瑞典 (© Martin Wahlborg/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.SourHerring_ZH-CN4136738467_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SourHerring_ZH-CN4136738467_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-17 如此清澈，如此洁净  

大自然的水族馆中的水下景观，巴西 (© Michel Roggo/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.AquarioNatural_ZH-CN3886634374_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.AquarioNatural_ZH-CN3886634374_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-16 惊险之旅  

夕阳下的大白鲨过山车，美国新泽西州 (© John Van Decker/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.GreatWhiteRoller_ZH-CN1541809088_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.GreatWhiteRoller_ZH-CN1541809088_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-15 古代中世纪城堡  

奇陶尔加尔堡，印度 (© Anand Purohit/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.ChittorgarhFort_ZH-CN2955182965_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com/th?id=OHR.ChittorgarhFort_ZH-CN2955182965_1920x1080.jpg&rf=LaDigue_1920x1080.jpg) 

## 2022-8-14 捉迷藏世界冠军  

琥珀山国家公园里的豹变色龙，马达加斯加 (© Christian Ziegler/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.PantherChameleon_ZH-CN2554514270_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.PantherChameleon_ZH-CN2554514270_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-13 一颗值得的心  

阿斯佩山谷中的蒙塔尼翁湖，法国 (© thieury/Adobe photo stock) [4k Edition](https://cn.bing.com//th?id=OHR.LacMontagnon_ZH-CN8301464080_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.LacMontagnon_ZH-CN8301464080_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-12 家庭出游  

安博塞利国家公园里的非洲象群，肯尼亚 (© Susan Portnoy/Shutterstock) [4k Edition](https://cn.bing.com//th?id=OHR.AmboseliElephants_ZH-CN2078609290_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.AmboseliElephants_ZH-CN2078609290_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-11 如果可以的话，请拥抱一座山吧  

日本长野县安昙野附近的燕岳山 (© Joshua Hawley/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.MtTsubakuro_ZH-CN0305525340_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.MtTsubakuro_ZH-CN0305525340_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-10 沙漠中的匕首？  

约书亚树，加利福尼亚州约书亚树国家公园 (© Tim Fitzharris/Minden Pictures) [4k Edition](https://cn.bing.com//th?id=OHR.AnniversaryJTNP_ZH-CN9974030692_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.AnniversaryJTNP_ZH-CN9974030692_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-9 9000年前的手印  

阿根廷圣克鲁斯的洛斯马诺斯洞穴 (© Adwo/Alamy) [4k Edition](https://cn.bing.com//th?id=OHR.CuevaManos_ZH-CN8900667928_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.CuevaManos_ZH-CN8900667928_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-8 汪洋中的小岛  

圣埃尔姆附近的潘塔留岛鸟瞰图，西班牙马略卡岛 (© Dimitri Weber/Azing航空公司) [4k Edition](https://cn.bing.com//th?id=OHR.EsPantaleu_ZH-CN8612029580_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.EsPantaleu_ZH-CN8612029580_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-7 贴秋膘了吗？  

杭州西湖的古典中国园林 (© DANNY HU/Getty Images) [4k Edition](https://cn.bing.com//th?id=OHR.theBeginningofAutumn2022_ZH-CN9413449297_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.theBeginningofAutumn2022_ZH-CN9413449297_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

## 2022-8-6 旧金山湾的盐滩  

旧金山湾的盐滩 (© Jeffrey Lewis/Tandem Stills + Motion) [4k Edition](https://cn.bing.com//th?id=OHR.SFSaltFlats_ZH-CN7261637239_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp)  

![](https://cn.bing.com//th?id=OHR.SFSaltFlats_ZH-CN7261637239_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp) 

